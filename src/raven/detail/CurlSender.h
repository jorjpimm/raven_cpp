#pragma once

#include "raven/sender_interface.h"

#include <curl/curl.h>

#include <atomic>
#include <deque>
#include <future>
#include <string>
#include <thread>


namespace raven
{

namespace curl
{

class GlobalCurlData
{
public:
  GlobalCurlData()
  {
    if (s_init_count++ == 0)
    {
      // In windows, this will init the winsock stuff
      curl_global_init(CURL_GLOBAL_ALL);
    }
  }

  ~GlobalCurlData()
  {
    if (--s_init_count == 0)
    {
      curl_global_cleanup();
    }
  }

  static std::atomic<std::size_t> s_init_count;
};

class Sender : public sender_interface
{
public:
  Sender();
  ~Sender();

	void setup_requests(const char *url, const char *header) override;

  struct Item
  {

    Item(std::chrono::seconds timeout);
    ~Item();

    std::string data_buffer;
    const char *data;

    std::chrono::seconds timeout;
    std::atomic<bool> complete;

		std::function<void(const response &)> callback;
    union
    {
      std::promise<response> promise;
      std::condition_variable completed_condition;
    };
  };
	
	response submit_item_sync(const char *data, std::chrono::seconds timeout) override;
	void submit_item(const char *data, std::chrono::seconds timeout, const std::function<void(const response &)> &callback) override;

private:
  void submit_item_result(const char *data);
  void sender_main();

  GlobalCurlData m_global;
  CURL *m_curl;
  curl_slist *m_headers;
  char error_buffer[CURL_ERROR_SIZE];

  std::thread m_thread;
  std::atomic<bool> m_exit;

  // List of items, memory is owned by the list, if the item is asyncronous.
  std::deque<std::shared_ptr<Item>> m_items;
  std::mutex m_item_mutex;
  std::condition_variable m_item_condition;

  // The current items response.
	response m_current_response;
};

}
}
