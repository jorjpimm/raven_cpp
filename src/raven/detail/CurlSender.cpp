#include "raven/detail/CurlSender.h"

#include <cassert>
#include <iostream>

namespace raven
{
namespace curl
{

std::atomic<std::size_t> GlobalCurlData::s_init_count(0);

Sender::Sender()
: m_curl(nullptr)
, m_headers(nullptr)
, m_exit(false)
{
}

Sender::~Sender()
{
  m_exit = true;
  m_item_condition.notify_one();
  m_thread.join();
  curl_slist_free_all(m_headers);
  curl_easy_cleanup(m_curl);
}

void Sender::setup_requests(const char *url, const char *header)
{
	if (m_curl)
	{
		throw std::runtime_error("Re initialising curl sender not supported");
	}

	m_curl = curl_easy_init();
	if (!m_curl)
	{
		throw std::runtime_error("Failed to initialise curl");
	}

	curl_easy_setopt(m_curl, CURLOPT_URL, url);

	m_headers = curl_slist_append(m_headers, "Content-Type: application/json");
	m_headers = curl_slist_append(m_headers, header);

	/* set our custom set of headers */
	auto res = curl_easy_setopt(m_curl, CURLOPT_HTTPHEADER, m_headers);
	if (res != CURLE_OK)
	{
		throw std::runtime_error("Failed to setup http header");
	}

	std::size_t(*data_received)(void *buffer, size_t size, size_t nmemb, void *stream);
	data_received = [](void *buffer, size_t size, size_t nmemb, void *stream)
	{
		assert(size == 1);
		Sender *ths = reinterpret_cast<Sender *>(stream);
		assert(ths);

		char *data = reinterpret_cast<char *>(buffer);
		ths->m_current_response.output.append(data, nmemb);

		return nmemb;
	};

	curl_easy_setopt(m_curl, CURLOPT_WRITEFUNCTION, data_received);
	curl_easy_setopt(m_curl, CURLOPT_WRITEDATA, this);
	curl_easy_setopt(m_curl, CURLOPT_ERRORBUFFER, error_buffer);

	m_thread = std::thread([&]() { sender_main(); });
}

Sender::response Sender::submit_item_sync(const char *data, std::chrono::seconds timeout)
{
  Item item(timeout);
  item.data = data;

	response resp;
	std::condition_variable completed_condition;
	item.callback = [&](const response &r)
	{
		resp = r;
		completed_condition.notify_one();
	};

  std::unique_lock<std::mutex> l(m_item_mutex);
  std::shared_ptr<Item> ptr(&item, [](Item *) {});
  m_items.push_back(ptr);
  m_item_condition.notify_one();

	completed_condition.wait(l, [&]() { return m_exit || item.complete; });
  return resp;
}

void Sender::submit_item(const char *data, std::chrono::seconds timeout, const std::function<void(const response &)> &callback)
{
	auto item = std::make_shared<Item>(timeout);

	item->callback = callback;

	item->data_buffer = data;
	item->data = item->data_buffer.c_str();

	std::unique_lock<std::mutex> l(m_item_mutex);
	m_items.push_back(item);
	m_item_condition.notify_one();
}

void Sender::sender_main()
{
  while (!m_exit || m_items.size())
  {
    std::unique_lock<std::mutex> l(m_item_mutex);
    m_item_condition.wait(l, [&]() { return m_exit || !m_items.empty(); });
    if (m_exit && m_items.empty())
    {
      return;
    }
  
    assert(m_items.size());
    auto item = m_items.front();
    m_items.pop_front();
    l.unlock();

		m_current_response.output.clear();
		m_current_response.http_response = 0;

    // Now specify the POST data
    curl_easy_setopt(m_curl, CURLOPT_POSTFIELDS, item->data);
    curl_easy_setopt(m_curl, CURLOPT_TIMEOUT, item->timeout.count());
  
    // Perform the request, res will get the return code
    auto res = curl_easy_perform(m_curl);
    if(res != CURLE_OK)
    {
      if(res == CURLE_OPERATION_TIMEDOUT)
      {
				m_current_response.http_response = 504;
      }
      else
      {
				m_current_response.http_response = 400;
      }
			m_current_response.output = error_buffer;
    }
    else
    {
      curl_easy_getinfo(m_curl, CURLINFO_RESPONSE_CODE, &m_current_response.http_response);
    }

		item->complete = true;
		if (item->callback)
		{
			item->callback(m_current_response);
		}
  }
}

Sender::Item::Item(std::chrono::seconds timeout)
: timeout(timeout)
, complete(false)
{
}

Sender::Item::~Item()
{
}

}
}
