#pragma once

#include <ostream>
#include <string>

namespace raven
{

class dsn
{
public:
	/// Create a new dsn from a string of the form:
	///   https://public:secret@sentry.example.com/1
	dsn(const char *dsn)
	: m_dsn(dsn)
	{
		parse_from(m_dsn);
	}

	/// Create a new dsn from a string of the form:
	///   https://public:secret@sentry.example.com/1
	dsn(const std::string &dsn)
	: m_dsn(dsn)
	{
		parse_from(m_dsn);
	}

	dsn(std::string &&dsn)
	: m_dsn(std::move(dsn))
	{
		parse_from(m_dsn);
	}

	/// The protocol of the dsn, for example: http
	std::string protocol() const
	{
		return m_protocol;
	}

	/// The host of the dsn, for example: sentry.example.com
	std::string host() const
	{
		return m_host;
	}

	/// The url of the dsn:
	///   https://sentry.example.com
	std::string uri() const
	{
		return protocol() + "://" + host();
	}

	/// The public key of the dsn
	std::string public_key() const
	{
		return m_public_key;
	}

	/// The secret key of the dsn
	std::string secret_key() const
	{
		return m_secret_key;
	}

	/// The project if of the dsn
	std::string project_id() const
	{
		return m_project_id;
	}

	/// Find the API URI from this dsn
	///   https://sentry.example.com/api/1/store/
	std::string api_location() const
	{
		return uri() + "/api/" + project_id() + "/store/";
	}

	void dump(std::ostream &str)
	{
		str << "DSN: " << m_dsn << std::endl;
		str << "  Protocol: " << protocol() << std::endl;
		str << "  Host: " << host() << std::endl;
		str << "  URI: " << uri() << std::endl;
		str << "  Public Key: " << public_key() << std::endl;
		str << "  Secret Key: " << secret_key() << std::endl;
		str << "  Project ID: " << project_id() << std::endl;
		str << "  API URI: " << api_location() << std::endl;

	}

private:
	void parse_from(const std::string &from)
	{
		auto colon = from.find("://");
		if (colon == std::string::npos)
		{
			throw std::invalid_argument("Invalid input dsn, expected protocol separated by ://");
		}
		m_protocol.assign(from.begin(), from.begin() + colon);
  
    auto key_start = colon + 3;
    auto key_end = from.find('@', key_start);
		if (key_start >= from.size() || key_end == std::string::npos)
    {
      throw std::invalid_argument("Invalid input dsn, expected key section with terminating @");
    }
  
		auto secret_start = from.find(":", key_start);
		if (secret_start == std::string::npos)
    {
      m_public_key.assign(from.begin() + key_start, from.begin() + key_end);
		}
    else
    {
      m_public_key.assign(from.begin() + key_start, from.begin() + secret_start);
      m_secret_key.assign(from.begin() + secret_start + 1, from.begin() + key_end);
    }

		auto host_start = key_end + 1;
		auto host_end = from.find("/", host_start);
		if (host_start >= from.size() || host_end == std::string::npos)
		{
			throw std::invalid_argument("Invalid input dsn, expected host terminated with /");
		}
		m_host.assign(from.begin() + host_start, from.begin() + host_end);

		auto project_start = host_end + 1;
		auto project_end = from.size();
		if (project_start >= from.size())
		{
			throw std::invalid_argument("Invalid input dsn, expected project");
		}
		m_project_id.assign(from.begin() + project_start, from.begin() + project_end);
	}

	std::string m_dsn;
	std::string m_protocol;
	std::string m_host;
	std::string m_public_key;
	std::string m_secret_key;
	std::string m_project_id;
};

}
