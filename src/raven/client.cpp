#include "raven/client.h"
#include "raven/detail/version.h"
#include "detail/CurlSender.h"

#include <array>
#include <chrono>
#include <ctime>

#include <boost/algorithm/string/replace.hpp>
#include <boost/asio/ip/host_name.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#ifdef _MSC_VER
# pragma warning(push)
# pragma warning(disable: 4100)
# pragma warning(disable: 4244)
#endif

#include <jsoncons/json.hpp>

#ifdef _MSC_VER
# pragma warning(pop)
#endif

namespace raven
{

struct client::impl
{
	enum class error
	{
		non_fatal,
		fatal
	};

	impl(
		const dsn &dsn,
		const std::string &name,
		const std::string &release,
		const tags &client_tags,
		sender_interface *inp_sender)
  : client_tags(client_tags)
  , client_dsn(dsn)
	, name(name)
	, release(release)
	, hostname(boost::asio::ip::host_name())
	, sender(inp_sender)
	{
    const auto now = std::chrono::system_clock::now();
		const auto epoch = now.time_since_epoch();
		const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(epoch);

		std::string version = std::to_string(RAVEN_CPP_VERSION_MAJOR) + "." +
			std::to_string(RAVEN_CPP_VERSION_MINOR) + "." +
			std::to_string(RAVEN_CPP_VERSION_REVISION);

	  std::string header("X-Sentry-Auth: ");
	  header += "Sentry sentry_version=5, ";
	  header += "sentry_timestamp=" + std::to_string(seconds.count()) +  ", ";
	  header += "sentry_key=" +  dsn.public_key() + ", ";
	  header += "sentry_client=raven-cpp/" + version + ", ";
	  header += "sentry_secret=" + dsn.secret_key();

		levels[std::size_t(level::fatal)] = "fatal";
		levels[std::size_t(level::error)] = "error";
		levels[std::size_t(level::warning)] = "warning";
		levels[std::size_t(level::info)] = "info";
		levels[std::size_t(level::debug)] = "debug";

		if (!sender)
		{
			owned_sender.reset(new curl::Sender());
			sender = owned_sender.get();
		}

		sender->setup_requests(dsn.api_location().c_str(), header.c_str());
	}

	jsoncons::json basic_event(
		const std::string &msg,
		level message_level)
	{
		auto id = boost::uuids::to_string(uuid_generator());
		boost::replace_all(id, "-", "");

		time_t now;
		time(&now);
		char buf[sizeof "2011-10-08T07:07:09Z"];
		strftime(buf, sizeof buf, "%FT%TZ", gmtime(&now));

		jsoncons::json msg_obj;
		msg_obj["event_id"] = id;
		msg_obj["message"] = msg;
		msg_obj["timestamp"] = (const char *)buf;
		msg_obj["level"] = levels[std::size_t(message_level)];
		msg_obj["logger"] = name;
		msg_obj["platform"] = "c";

		msg_obj["release"] = release;
		msg_obj["server_name"] = hostname;

		return msg_obj;
	}

	jsoncons::json make_tags(
		std::initializer_list<const tags> tags)
	{
		auto tags_obj = jsoncons::json::array();

		for (auto &tag_list : tags)
		{
			for (auto &tag : tag_list)
			{
				auto pair = jsoncons::json::array();
				pair.reserve(2);
				pair.push_back(tag.first);
				pair.push_back(tag.second);

				tags_obj.push_back(pair);
			}
		}
		return tags_obj;
	}

	void handle_response(const sender_interface::response &resp)
	{
		if (resp.http_response != 200)
		{
			handle_error(error::non_fatal, resp.output);
		}
	}

	void handle_error(error, const std::string &)
	{
		assert(false);
	}

	const tags client_tags;
	const dsn client_dsn;
	const std::string name;
	const std::string release;
	const std::string hostname;
	sender_interface *sender;
	std::unique_ptr<sender_interface> owned_sender;
  boost::uuids::random_generator uuid_generator;
	std::array<std::string, std::size_t(client::level::level_count)> levels;
};

client::client(
	const dsn &dsn,
	const std::string &name,
	const std::string &release,
	const tags &client_tags,
	sender_interface *sender)
: m_impl(new impl(dsn, name, release, client_tags, sender))
{
}

client::~client()
{
}

void client::capture_message(
	const std::string &msg,
	const tags &message_tags,
	level message_level)
{
	auto msg_obj = m_impl->basic_event(msg, message_level);
	msg_obj["tags"] = m_impl->make_tags({ message_tags, m_impl->client_tags });

	m_impl->sender->submit_item(
		msg_obj.to_string().c_str(),
		std::chrono::seconds(5),
		[&](const sender_interface::response &r) { m_impl->handle_response(r); }
		);
}

void client::capture_exception(
	const std::exception_ptr &ptr,
	const tags &message_tags,
	level message_level)
{
	std::string message;
	try {
		if (!ptr) {
			m_impl->handle_error(impl::error::non_fatal, "Invalid exception_ptr specified.");
			return;
		}
		std::rethrow_exception(ptr);
	}
	catch (...) {
		message = boost::current_exception_diagnostic_information();
	}

	auto msg_obj = m_impl->basic_event(message, message_level);
	msg_obj["tags"] = m_impl->make_tags({ message_tags, m_impl->client_tags });

	m_impl->sender->submit_item(
		msg_obj.to_string().c_str(),
		std::chrono::seconds(5),
		[&](const sender_interface::response &r) { m_impl->handle_response(r); }
		);
}

void client::capture_exceptions(
	std::function<void()> func,
	exception_mode mode,
	const tags &message_tags,
	level message_level)
{
	try
	{
	 func();
	}
	catch (...)
	{
		capture_exception(std::current_exception(), message_tags, message_level);
		if (mode == exception_mode::rethrow)
		{
			throw;
		}
	}
}

}
