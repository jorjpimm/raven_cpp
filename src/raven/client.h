#pragma once
#include "raven/dsn.h"
#include "raven/tags.h"

#include <functional>
#include <memory>

namespace raven
{

class sender_interface;
class tags;

class client
{
public:
	/// The level of the message passed to sentry.
	enum class level
	{
		fatal,
		error,
		warning,
		info,
		debug,

		level_count
	};

	/// Shows how the client handle exceptions it is passed, either
	/// by rethrowing after reporting, or discarding after reporting.
	enum class exception_mode
	{
		absorb,
		rethrow
	};

	/// Create a new client, pointing at the passed [dsn].
	/// \param name The name of the logger being created.
	/// \param release Identifier for the currently running software (eg. commit hash).
	/// \param client_tags Tags which should be sent with every message the client sends.
	/// \param sender An overriding sender interface, to use rather than the internal default.
	client(
		const dsn &dsn,
		const std::string &name = std::string(),
		const std::string &release = std::string(),
		const tags &client_tags = tags::empty(),
		sender_interface *sender = nullptr);
	~client();

	/// Send a message.
	/// \note The message is passed asynchronously.
	/// \param msg The message to send
	/// \param message_tags Tags to combine with the client tags and send with this message
	/// \param message_level The level to send with this message.
	void capture_message(
		const std::string &msg,
		const tags &message_tags = tags::empty(),
		level message_level = level::error);

	/// Send the passed exception.
	/// \param ptr The exception to send.
	/// \param message_tags Tags to combine with the client tags and send with this message
	/// \param message_level The level to send with this message.
	void capture_exception(
		const std::exception_ptr &ptr,
		const tags &message_tags = tags::empty(),
		level message_level = level::error);

	/// Run the passed function and if an exception occurs, send it immediately.
	/// \param func The function to run.
	/// \param mode The mode to treat the exception with if one occurs.
	/// \param message_tags Tags to combine with the client tags and send with this message
	/// \param message_level The level to send with this message.
	void capture_exceptions(
		std::function<void()> func,
		exception_mode mode = exception_mode::absorb,
		const tags &message_tags = tags::empty(),
		level message_level = level::error);

private:
	struct impl;
	std::unique_ptr<impl> m_impl;
};

}
