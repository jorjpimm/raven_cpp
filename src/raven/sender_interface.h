#pragma once

#include <chrono>
#include <future>
#include <string>

namespace raven
{

class sender_interface
{
public:
  struct response
  {
    std::string output;
    std::int32_t http_response;
  };

	virtual ~sender_interface() {}

	virtual void setup_requests(const char *url, const char *header) = 0;

  virtual response submit_item_sync(const char *data, std::chrono::seconds timeout) = 0;
	virtual void submit_item(const char *data, std::chrono::seconds timeout, const std::function<void(const response &)> &callback) = 0;
};

}
