#pragma once

#include <ostream>
#include <string>
#include <vector>

namespace raven
{

class tags
{
public:
	using container = std::vector<std::pair<std::string, std::string>>;

  static tags empty() {
    return tags();
  }

	tags &add(const std::string &key, const std::string &value)
	{
		_tags.push_back(std::make_pair(key, value));
		return *this;
	}

	container::const_iterator begin() const { return _tags.begin(); }
	container::const_iterator end() const { return _tags.end(); }

private:
	std::vector<std::pair<std::string, std::string>> _tags;
};

}
