Dependencies
------------

You will need:

* CMake >= 3.2
  * Windows users can get CMake from https://cmake.org/
  * OSX users can `brew install cmake`
* libcurl
  * Can be obtained on windows from https://curl.haxx.se/dlwiz/, on x64 you may have to build from source.
  * OSX users can use `brew install libcurl`
* Boost
  * Can be obtained on windows from http://www.boost.org/
  * OSX users can use `brew install boost`

Building
--------

From the source directory run:

```
  > mkdir build
  > cd build

  # First generate a project using cmake

  # On osx or linux
  > cmake ..

  # On Windows
  > CURL_LOCATION=WHERE_YOU_PUT_LIBCURL
  > cmake -A x64 -D${CURL_LOCATION}/lib/libcurl.lib -DCURL_INCLUDE_DIR=${CURL_LOCATION}/include/ ..

  # Then build the generated project
  > cmake --build .

```

Testing
-------

Send a verification message using:

```
  > raven_client msg --dsn http://a:b@sentry.server.com/project --msg test_message
```
