
add_executable(raven_cpp_test
  main.cpp
  client_test.cpp
  dsn_test.cpp
)

target_link_libraries(raven_cpp_test
  raven
)


target_include_directories(raven_cpp_test SYSTEM
  PRIVATE
    ${CMAKE_SOURCE_DIR}/external
    ${Boost_INCLUDE_DIR}
)

add_test(
  NAME raven_cpp_test
  COMMAND raven_cpp_test
)

set_property(TARGET raven_cpp_test
  PROPERTY
    CXX_STANDARD 11
)
