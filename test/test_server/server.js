var express        =        require("express");
var bodyParser     =        require("body-parser");
var app            =        express();
//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/api/test/store',function(request,response){
  console.log("got a thing:");
  console.log(request);
  console.log(request.headers);
  console.log(request.body);
  response.send("pork");
});

app.listen(3000,function(){
  console.log("Started on PORT 3000");
});
