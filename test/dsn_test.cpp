#include "raven/dsn.h"

#include <catch/catch.hpp>

SCENARIO("dsn is parsed correcly", "[dsn]")
{
  WHEN("constructing dsn from bad strings")
  {
    THEN("throws exception")
    {
      REQUIRE_THROWS(raven::dsn(""));
      REQUIRE_THROWS(raven::dsn("http://x.com/1"));
      REQUIRE_THROWS(raven::dsn("http:/a@x.com/1"));
    }
  }
  
  WHEN("dsn has valid string")
  {
    raven::dsn dsn("http://a:b@www.xyz.pork.com/1-x23");
  
    THEN("dsn has correct paramters")
    {
      REQUIRE(dsn.secret_key() == "b");
      REQUIRE(dsn.public_key() == "a");
      REQUIRE(dsn.protocol() == "http");
      REQUIRE(dsn.host() == "www.xyz.pork.com");
      REQUIRE(dsn.project_id() == "1-x23");
    }
  }
  
  WHEN("dsn has no private string")
  {
  raven::dsn dsn("https://32414325443aabf@app.getsentry.com/245");
  
  THEN("dsn has correct paramters")
    {
    REQUIRE(dsn.secret_key() == "");
    REQUIRE(dsn.public_key() == "32414325443aabf");
    REQUIRE(dsn.protocol() == "https");
    REQUIRE(dsn.host() == "app.getsentry.com");
    REQUIRE(dsn.project_id() == "245");
    }
  }

  WHEN("dsn has port")
  {
    raven::dsn dsn("http://a:b@x.com:89/1");
  
    THEN("dsn has correct paramters")
    {
      REQUIRE(dsn.secret_key() == "b");
      REQUIRE(dsn.public_key() == "a");
      REQUIRE(dsn.protocol() == "http");
      REQUIRE(dsn.host() == "x.com:89");
      REQUIRE(dsn.project_id() == "1");
    }
  }
}
