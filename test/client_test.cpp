#include "raven/client.h"
#include "raven/sender_interface.h"
#include "raven/detail/version.h"

#ifdef _MSC_VER
# pragma warning(push)
# pragma warning(disable: 4100)
# pragma warning(disable: 4244)
# pragma warning(disable: 4515)

#elif __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wzero-length-array"
#endif

#include <boost/algorithm/string/predicate.hpp>
#include <catch/catch.hpp>
#include <fakeit/fakeit.hpp>
#include <jsoncons/json.hpp>

#ifdef _MSC_VER
# pragma warning(pop)

#elif __clang__
#pragma clang diagnostic pop
#endif

SCENARIO("client correctly sends messages", "[client]")
{
  WHEN("given a default client")
  {
		fakeit::Mock<raven::sender_interface> sender;

		fakeit::When(Method(sender, setup_requests)).Do([](const char *url, const char *headers) {
			std::string version = std::to_string(RAVEN_CPP_VERSION_MAJOR) + "." + std::to_string(RAVEN_CPP_VERSION_MINOR) + "." + std::to_string(RAVEN_CPP_VERSION_REVISION);
			REQUIRE(std::string(url) == "http://www.xyz.pork.com/api/1-x23/store/");
			REQUIRE(boost::starts_with(headers, "X-Sentry-Auth: Sentry sentry_version=5, sentry_timestamp="));
			REQUIRE(boost::ends_with(headers, ", sentry_key=a, sentry_client=raven-cpp/" + version + ", sentry_secret=b"));
		});

    raven::client client(
			"http://a:b@www.xyz.pork.com/1-x23",
			std::string(),
			std::string(),
			raven::tags::empty(),
			&sender.get());

		WHEN("Sending message")
		{
			fakeit::When(Method(sender, submit_item)).Do([](const char *data, std::chrono::seconds timeout, const std::function<void(const raven::sender_interface::response &)> &callback) {
				REQUIRE(timeout == std::chrono::seconds(5));

				auto parsed = jsoncons::json::parse_string(data);
				REQUIRE(parsed["message"].as<std::string>() == "TEST");

				raven::sender_interface::response resp;
				resp.http_response = 200;
				resp.output = "{}";
				callback(resp);
			});

			client.capture_message("TEST");
		}
  }
}
