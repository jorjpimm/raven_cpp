
add_executable(raven_client
  main.cpp
)

target_compile_definitions(raven_client
  PRIVATE
    BOOST_ALL_NO_LIB
)

target_include_directories(raven_client
  PRIVATE
    ${Boost_INCLUDE_DIR}
)

target_link_libraries(raven_client
  raven
  ${Boost_PROGRAM_OPTIONS_LIBRARY}
)

set_property(TARGET raven_client
  PROPERTY
    CXX_STANDARD 11
)
