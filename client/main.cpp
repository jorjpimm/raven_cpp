#include "raven/dsn.h"
#include "raven/client.h"

#ifdef _MSC_VER
# pragma warning(push)
# pragma warning(disable: 4505)
#endif

#include <iostream>
#include <unordered_map>
#include "boost/program_options.hpp"
#include "boost/exception/diagnostic_information.hpp"
#include "boost/exception/errinfo_errno.hpp"

#ifdef _MSC_VER
# pragma warning(pop)
#endif

namespace po = boost::program_options;

using SubArguments = std::vector<std::string>;
struct Options
{
  bool debugging = false;
};

po::variables_map parse_arguments(
	const SubArguments &argv,
	po::options_description &opts,
  const po::positional_options_description &pos = po::positional_options_description())
{
	opts.add_options()
		("help,h", "print help message");
	po::variables_map vm;
	po::store(
		po::command_line_parser(argv)
			.options(opts)
			.positional(pos)
			.run(),
		vm);
	po::notify(vm);

	if (vm.count("help"))
	{
		opts.print(std::cout);
		exit(0);
	}
	return vm;
}

int main(int argc, char **argv)
{
  std::unordered_map<std::string, std::function<void(const SubArguments &)>> cmds;
  cmds["dsn"] = [](const SubArguments &opts)
  {
	  std::string dsn_str;

	  po::options_description desc("dst options");
	  desc.add_options()
		  ("dsn", po::value(&dsn_str), "dsn to send to");
	  po::positional_options_description pos_desc;
	  pos_desc.add("dsn", 1);
	  parse_arguments(opts, desc, pos_desc);

	  raven::dsn d(dsn_str);
	  d.dump(std::cout);
  };

	cmds["msg"] = [](const SubArguments &opts)
	{
		std::string dsn_str;
		std::string msg_str;

		po::options_description desc("dst options");
		desc.add_options()
			("dsn", po::value(&dsn_str), "dsn to send to")
			("msg", po::value(&msg_str), "message to send");
		parse_arguments(opts, desc);

		raven::tags tags;
		tags.add("test1", "a")
			.add("test2", "b");
		raven::client client(dsn_str, "test_client", "abcdef1", tags);

		raven::tags tags_msg;
		tags_msg.add("test1", "other");

		client.capture_message("test", tags_msg);
	};

	cmds["exception"] = [](const SubArguments &opts)
	{
		std::string dsn_str;
		std::string msg_str;

		po::options_description desc("dst options");
		desc.add_options()
			("dsn", po::value(&dsn_str), "dsn to send to")
			("msg", po::value(&msg_str), "message to send");
		parse_arguments(opts, desc);

		raven::tags tags;
		tags.add("test1", "a")
			.add("test2", "b");
		raven::client client(dsn_str, "test_client", "abcdef1", tags);

		raven::tags tags_msg;
		tags_msg.add("test1", "other");

    class test_exception : public virtual boost::exception, public virtual std::exception
    {
      const char* what() const noexcept override { return "Test exception"; }
    };
  
		client.capture_exceptions(
      [](){
        BOOST_THROW_EXCEPTION(test_exception()
                              << boost::errinfo_errno(5)
                              << boost::errinfo_errno(5)
        );
			},
			raven::client::exception_mode::absorb,
			tags_msg);
	};


	try
	{
		Options opt;

		std::string command;
		SubArguments sub_arguments;

		po::options_description global("Global options");
		global.add_options()
			("help,h", "print help message")
			("debug", po::bool_switch(&opt.debugging), "Turn on debug output")
			("command", po::value(&command), "command to execute")
			("subargs", po::value(&sub_arguments), "Arguments for command");

		po::positional_options_description pos;
		pos.add("command", 1).
			add("subargs", -1);

		po::parsed_options parsed = po::command_line_parser(argc, argv).
			options(global).
			positional(pos).
			allow_unregistered().
			run();

		po::variables_map vm;
		po::store(parsed, vm);
		po::notify(vm);

		// Collect all the unrecognized options from the first pass. This will include the
		// (positional) command name, so we need to erase that.
		std::vector<std::string> opts = po::collect_unrecognized(parsed.options, po::include_positional);
		if (opts.empty())
		{
			if (opts.empty())
			{
				std::cerr << "No command specified" << std::endl;
			}
			global.print(std::cout);
			exit(0);
		}

		opts.erase(opts.begin());
		if (vm.count("help"))
		{
			opts.push_back("--help");
		}

		auto cmd = cmds.find(command);
		if (cmd == cmds.end())
		{
			std::cerr << "Invalid command " << command << "specified" << std::endl;
			exit(EXIT_FAILURE);
		}

		cmd->second(opts);
	}
	catch (...)
	{
		std::cerr << "Unhandled exception!" << std::endl <<
			boost::current_exception_diagnostic_information();
		return EXIT_FAILURE;
	}

  return EXIT_SUCCESS;
}
